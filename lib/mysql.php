<?php
function disconnectDatabase($con)
{
    mysqli_close($con);
}

function connectDatabaseUsingEnv()
{
    $url = getenv('DATABASE_URL');
    $username = getenv('DATABASE_USERNAME');
    $password = getenv('DATABASE_PASSWORD');
    $dbName = getenv('DATABASE_NAME');

    return mysqli_connect($url, $username, $password, $dbName);
}

function selectUsers($username = null)
{
    $conditions = [];
    if ($username !== null) {
        $conditions['username'] = $username;
    }

    return selectRecords('users', $conditions);
}

function countUsers($username = null)
{
    $conditions = [];
    if ($username !== null) {
        $conditions['username'] = $username;
    }

    return countRecords('users', $conditions);
}

function insertUser($username, $password)
{
    $hash = password_hash($password, PASSWORD_DEFAULT);
    $sql = "INSERT INTO users (username, password) VALUES('";
    $sql .= $username . "', '";
    $sql .= $hash . "');";

    return executeSql($sql);
}

function selectRecords($table, $conditions = [])
{
    $sql = 'SELECT * FROM ' . $table;

    return executeSql($sql, $conditions);
}

function countRecords($table, $conditions = [])
{
    $sql = 'SELECT count(*) as count_users FROM ' . $table;

    return implode(' ', executeSql($sql, $conditions));
}

function executeSql($sql, $conditions = [])
{
    $con = connectDatabaseUsingEnv();
    foreach ($conditions as $key => $value) {
        $sql .= ' WHERE ' . $key . ' = "' . $value . '"';
    }

    $query = mysqli_query($con, $sql);
    disconnectDatabase($con);

    return mysqli_fetch_assoc($query);
}
