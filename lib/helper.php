<?php
function makeid($length)
{
    $result           = '';
    $characters       = 'abcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    for ($i = 0; $i < $length; $i++) {
        $result .= $characters[rand(0, $charactersLength - 1)];
    }
    $result .= '@gmail.com';

    return $result;
}
