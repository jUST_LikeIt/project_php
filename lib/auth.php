<?php
function auth($password, $inputPassword)
{
    if (password_verify($inputPassword, $password)) {
        return true;
    }
    return false;
}

function confirmPassword($password, $confirmPassword)
{
    if (strcmp($password, $confirmPassword) == 0) {
        return true;
    }
    return false;
}
