<?php
function redirect($path)
{
    $header = 'Location: ./' . $path . '.php';
    header($header);
}

function redirectToLogin()
{
    redirect('login');
}

function redirectToIndex()
{
    redirect('index');
}

function redirectToRegister()
{
    redirect('register');
}
