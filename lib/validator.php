<?php
function isValidEmail($username)
{
    if (preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/', $username)) {
        return true;
    }
    return false;
}
