<?php
require_once '../environment.php';
require_once '../lib/auth.php';
require_once '../lib/mysql.php';
require_once '../lib/helper.php';
require_once '../lib/redirect.php';
require_once '../lib/validator.php';

$username = makeid(5);
$password = 'zxczxc';
$confirmPassword = 'zxczxc';

$errorUsername = null;
$errorPassword = null;
$errorConfirmPassword = null;

if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    $confirmPassword = trim($_POST['confirmPassword']);

    if (!isValidEmail($username)) {
        $errorUsername = 'Username is invalid!';
    } elseif (strlen($password) <= 5) {
        $errorPassword = 'Password is invalid!';
    } elseif (!confirmPassword($password, $confirmPassword)) {
        $errorConfirmPassword = 'Confirm password does not match!';
    }

    if (!isset($errorUsername) && !isset($errorPassword) && !isset($errorConfirmPassword)) {
        $user = selectUsers($username);
        if ($user == null) {
            $userInsert = insertUser($username, $password);
            if ($userInsert == null) {
                $errorInsertUser = 'register is fail!';
                redirectToRegister();
            }
            $_SESSION['username'] = $username;
            redirectToLogin();
        } else {
            $userIdExist = 'user is exist!';
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <style type="text/css">
        .error { color: #F00 }
    </style>
</head>
<body>
    <?php if (isset($errorInsertUser)) { ?>
        <div class="error"><?= $errorInsertUser ?></div>
    <?php } ?>
    <?php if (isset($userIdExist)) { ?>
        <div class="error"><?= $userIdExist ?></div>
    <?php } ?>
    <form action="./register.php" method="post">
        <h1>Register</h1>
        <?php if (isset($error)) { ?>
            <div class="error"><?= $error ?></div>
        <?php } ?>
        <div>
            <label for="username">Name</label>
            <input name="username"value="<?= $username ?>">
            <?php if (isset($errorUsername)) { ?>
                <div class="error"><?= $errorUsername ?></div>
            <?php } ?>
        </div>
        <div>
            <label for="password">Password</label>
            <input type="password" name="password" value="<?= $password ?>">
            <?php if (isset($errorPassword)) { ?>
                <div class="error"><?= $errorPassword ?></div>
            <?php } ?>
        </div>
        <div>
            <label for="confirmPassword">Confirm Password</label>
            <input type="password" name="confirmPassword" value="<?= $confirmPassword ?>">
            <?php if (isset($errorConfirmPassword)) { ?>
                <div class="error"><?= $errorConfirmPassword ?></div>
            <?php } ?>
        </div>
        <div>
            <button>Register</button>
        </div>
    </form>
    <div>
        Đã có tài khoản? <a href="./login.php">Đăng nhập</a>
    </div>
</body>
</html>