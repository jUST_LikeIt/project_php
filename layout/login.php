<?php
require_once '../environment.php';
require_once '../lib/auth.php';
require_once '../lib/mysql.php';
require_once '../lib/redirect.php';

session_start();

if (isset($_SESSION['username'])) {
    redirectToIndex();
}

if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    $user = selectUsers($username);

    if ($user == null) {
        $errorUsername = 'User does not exist!';
    } elseif (auth($user['password'], $password)) {
        $_SESSION['username'] = $username;
        redirectToIndex();
    } else {
        $errorPassword = 'Password is not correct!';
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <style type="text/css">
        .error { color: #F00 }
    </style>
</head>
<body>
    <form action="./login.php" method="post">
        <h1>Login</h1>
        <div>
            <label for="username">Name</label>
            <input name="username" id="username">
            <?php if (isset($errorUsername)) { ?>
                <div class="error"><?= $errorUsername ?></div>
            <?php } ?>
        </div>
        <div>
            <label for="password">Password</label>
            <input type="password" name="password" id="password" value="zxczxc">
            <?php if (isset($errorPassword)) { ?>
                <div class="error"><?= $errorPassword ?></div>
            <?php } ?>
        </div>
        <div>
            <button>Login</button>
        </div>
    </form>
    <div>
        Bạn chưa có tài khoản? <a href="./register.php">Đăng ký ngay</a>
    </div>
</body>
</html>
