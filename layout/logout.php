<?php
require_once '../lib/redirect.php';

session_start();

unset($_SESSION['username']);
redirectToLogin();
