<?php
require_once '../lib/redirect.php';

session_start();
if ((!isset($_SESSION['username']))) {
    redirectToLogin();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>
</head>
<body>
    <p>Welcome back, <label><?= $_SESSION['username']?></label></p>
    <a href="./logout.php"><button>Logout</button></a>
</body>
</html>